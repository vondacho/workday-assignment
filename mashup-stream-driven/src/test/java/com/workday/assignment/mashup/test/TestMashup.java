package com.workday.assignment.mashup.test;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.Gson;
import com.workday.assignment.mashup.Mashup;
import com.workday.assignment.mashup.ProjectSummary;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={TestMashupConfig.class})
public class TestMashup {
		
	@Autowired
	Mashup mashup;
	
	@SuppressWarnings("unchecked")
	@Test
	public void mashupE2ETest() {
		String json = mashup.process(TestMashupConfig.searchTerm);
		ProjectSummary[] ps = new Gson().fromJson(json, ProjectSummary[].class);
		Assert.assertEquals(ps[0].getProject(), TestMashupConfig.projectFullName);
		Assert.assertEquals(((Map<String,String>)ps[0].getTweets().get(0)).get(TestMashupConfig.tweetTextKey), TestMashupConfig.tweetText);
	}
}
