package com.workday.assignment.mashup;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Mashup of the GitHub and Twitter APIs.
 * Search for projects on GitHub, then for each project search for tweets that mention it
 * 
 * @author olivier
 */
@Component
public class Mashup {
	
	@Autowired
	private GitHubSearch githubSearch;
	@Autowired
	private TwitterSearch twitterSearch;

	@Value("${twitterParallelRequestsCountLimit:100}")
	private Integer twitterParallelRequestsCountLimit;
	
	/**
	 * Mashup pipeline
	 * 
	 * @param topic a term as input for the project search inside GitHub
	 * @return a summary of each project with a short list of recent tweets, in json format
	 */
	public String process(String topic) {
		Executor executor = createExecutor(twitterParallelRequestsCountLimit);
		Gson gson = createJsonSerializer();

		return
			// Transformer: serializes the output of the pipeline as a JSON string
			gson.toJson(
			// Source: input of the pipeline as a one element stream
			Stream.of(topic).
			// Processor: GitHub search on the given term in input returns a stream of projects
			flatMap(ghTerm -> StreamSupport.stream(githubSearch.process(ghTerm).spliterator(), false)).
			// Filter: eliminates duplicate projects based on project name
			distinct().
			// Splitter: one parallel Twitter request by project. Initiates a stream of project summaries
			map(ghRepo -> CompletableFuture.supplyAsync(() ->
			// Enricher: decorates the Twitter response with the full name of the project
			new ProjectSummary(ghRepo.getFullName(), 
			// Processor: Asynchronous Twitter search on project name returns a list of tweets
			twitterSearch.process(ghRepo.getName())), executor)).
			// Joiner: waits until all the Twitter search have finished and returns a stream of project summaries
			map(CompletableFuture::join).
			// Transformer: sorts the stream of summaries by natural ordering (project name)
			sorted().
			// Aggregator: collects the element of the resulting stream into a list of project summaries
			collect(Collectors.toList()));
	}
	
	/**
	 * @return a JSON serializer 
	 */
	private Gson createJsonSerializer() {
		return TweetSerializer.register(new GsonBuilder()).create();
	}

	static final int THREAD_POOL_SIZE_LIMIT = 100;
	
	/**
	 * @return a thread pool executor for parallel processing
	 */
	private Executor createExecutor(int desiredSize) {
		return Executors.newFixedThreadPool(Math.min(desiredSize, THREAD_POOL_SIZE_LIMIT), new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				// Deamon threads are automatically killed on program termination
				t.setDaemon(true);
				return t;
			}
		});
	}
}
