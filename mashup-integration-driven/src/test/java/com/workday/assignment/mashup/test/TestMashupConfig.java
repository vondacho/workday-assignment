package com.workday.assignment.mashup.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

import com.workday.assignment.mashup.GHRepo;
import com.workday.assignment.mashup.GitHubSearch;
import com.workday.assignment.mashup.TwitterSearch;
import com.workday.assignment.mashup.Mashup;

/**
 * Configuration of our E2E test
 * GitHub and Twitter search components are mocked to return an expected result only
 * 
 * @author olivier
 */
@Configuration
public class TestMashupConfig {

	static final String searchTerm = "test";
	static final String projectName = "testRepo";
	static final String projectFullName = "testRepo/testRepo";
	static final String tweetTextKey = "text";
	static final String tweetText = "testText";
	static final String tweetJson = "{\"" + tweetTextKey + "\":\"" + tweetText + "\"}";
	
	@Bean
	public Mashup mashup() throws Exception {
		return new Mashup();
	}
	
	@Bean
	public GitHubSearch ghSearch() {
		GitHubSearch ghMock = mock(GitHubSearch.class);
		GHRepo ghRepoMock = mock(GHRepo.class);
		when(ghRepoMock.getName()).thenReturn(projectName);
		when(ghRepoMock.getFullName()).thenReturn(projectFullName);
		when(ghMock.process(searchTerm)).thenReturn(Collections.singletonList(ghRepoMock));
		return ghMock;
	}

	@Bean
	public TwitterSearch twSearch() throws TwitterException {
		TwitterSearch twMock = mock(TwitterSearch.class);
		Status twStatusMock = TwitterObjectFactory.createStatus(tweetJson);
		when(twMock.process(projectName)).thenReturn(Collections.singletonList(twStatusMock));
		return twMock;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
