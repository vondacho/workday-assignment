package com.workday.assignment.mashup;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.workday.assignment.config.ApplicationConfig;

/**
 * Main program
 * 
 * Environment MAY define the configDir variable where to find the user.properties file
 * 
 * @author olivier
 */
public class Application {
	static final Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * Main program
	 * @param args a term as input for the project search inside GitHub and optionally a file where to output the results
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			try {
				// Configuration
				AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
				// MashUp
				String result = context.getBean(Mashup.class).process(args[0]);
				// Cleanup
				context.close();
				
				// Logging results
				logger.debug(result);
				// Output into a given file
				if (args.length > 1) {
					BufferedWriter bw = Files.newBufferedWriter(Paths.get(args[1]));
					bw.write(result);
					bw.flush();
					bw.close();
				}
				// Output on the standard output
				else System.out.println(result);
			}
			catch (Throwable e) {
				if (e.getCause() instanceof FileNotFoundException)
					System.out.println("The user.properties configuration file is missing. The default path is ./user.properties");
				else
					System.out.println("An error occurred during the execution of the program: " + e.getMessage());
			}
		}
		else System.out.println("The search term argument is missing.");
	}
}
