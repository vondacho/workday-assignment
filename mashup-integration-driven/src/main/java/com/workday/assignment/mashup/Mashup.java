package com.workday.assignment.mashup;

import java.util.List;

import org.apache.camel.CamelContext;
import org.apache.camel.Consumer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.processor.aggregate.AbstractListAggregationStrategy;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.GsonBuilder;

/**
 * Mashup of the GitHub and Twitter APIs.
 * Search for projects on GitHub, then for each project search for tweets that mention it
 * 
 * @author olivier
 */
@Component
public class Mashup {
	static final Logger logger = LoggerFactory.getLogger(Mashup.class);
	
	@Autowired
	private GitHubSearch githubSearch;
	@Autowired
	private TwitterSearch twitterSearch;
	
	/**
	 * Mashup pipeline
	 * 
	 * @param topic a term as input for the project search inside GitHub
	 * @return a summary of each project with a short list of recent tweets, in json format
	 */
	public String process(String topic) throws Exception {
		CamelContext camelContext = new DefaultCamelContext();
		
		// configures mashup route
		camelContext.addRoutes(new RouteBuilder() {
		    public void configure() {
		    	// start of the route, receives topic
		        from("direct:start").
				// Processor: GitHub search for the given body
		        process(githubSearch()).
				// Splitter: one Twitter request by GitHub project.
		        split(body()).
		        // Aggregation of the resulting project summaries into a list
		        aggregationStrategy(projectSummaryAggregationStrategy()).
		        // Parallel processing of the Twitter requests
		        parallelProcessing(true).
		        // Saves the full name of the project in the header for future use
	        	setHeader("projectFullName", simple("${body.fullName}")).
	        	// Extract the name of the project
		        transform(simple("${body.name}")).
				// Processor: Twitter search on project name, returns a list of tweets
		        process(twitterSearch()).
				// Transformer: project summary bundling
		        process(projectSummary()).
		        // End of splitting
		        end().
				// Transformer: serializes the aggregated project summaries to a JSON string
		        process(jsonSerializer()).
		        // End of the route
		        to("direct:end");
		    }
		});
		
		// container for the final result
		StringBuilder result = new StringBuilder();
		// producer for injecting the topic into the route
		ProducerTemplate topicProducer = camelContext.createProducerTemplate();
		// consumer for getting the final result from the end of the route
		Consumer resultConsumer = camelContext.getEndpoint("direct:end").createConsumer(new Processor() {
	        public void process(Exchange exchange) throws Exception {
	        	result.append(exchange.getIn().getBody(String.class));
	        }
	    });
		resultConsumer.start();
		// starts Camel process
		camelContext.start();
		// injects the topic
		topicProducer.sendBody("direct:start", topic);
		// End of the Camel process
		camelContext.stop();

		return result.toString();
	}
	
	/**
	 * Message in: search topic as string
 	 * Message out: iterator<?>
	 * @return a GitHubClient as processor
	 */
	private Processor githubSearch() {
		return new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setBody(githubSearch.process(exchange.getIn().getBody(String.class)).iterator());
            }
        };
	}
	
	/**
	 * Header in/out: full name of the project
	 * Message in: name of the project
 	 * Message out: List of tweets
	 * @return a TwitterClient as processor
	 */
	private Processor twitterSearch() {
		return new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setBody(twitterSearch.process(exchange.getIn().getBody(String.class)));
                exchange.getOut().setHeader("projectFullName", exchange.getIn().getHeader("projectFullName", String.class));
           }
        };
	}

	/**
	 * Header in: full name of the project
 	 * Message in: List of tweets
 	 * Message out: a project summary(full name, tweets)
	 * @return a project summary bundler as transformer
	 */
	private Processor projectSummary() {
		return new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setBody(
            		new ProjectSummary(
        				exchange.getIn().getHeader("projectFullName", String.class),
        				exchange.getIn().getBody(List.class)));
           }
        };
	}

	/**
 	 * Message in: aggregated list of project summaries
 	 * Message out: the input as JSON payload
	 * @return a JSON serializer as processor
	 */
	private Processor jsonSerializer() {
		return new Processor() {
            public void process(Exchange exchange) throws Exception {
                exchange.getOut().setBody(
            		TweetSerializer.register(new GsonBuilder()).create().
            		toJson(exchange.getIn().getBody(List.class)));
           }
        };
	}
	
	/**
	 * @return the strategy for the aggregation of the twitter responses
	 */
	private AggregationStrategy projectSummaryAggregationStrategy() {
		return new AbstractListAggregationStrategy<ProjectSummary>() {
			@Override
			public ProjectSummary getValue(Exchange exchange) {
				return exchange.getIn().getBody(ProjectSummary.class);
			}
		};
    }
}