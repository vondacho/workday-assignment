package com.workday.assignment.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Creation and configuration of main components
 * 
 * default.properties are located on the classpath and embedded in the application
 * user.properties are required to be accessible on the file system
 * 
 * @author olivier
 */
@Configuration
@PropertySources({
    @PropertySource("classpath:config/default.properties"),
    @PropertySource("file:${configDir:.}/user.properties")
})
@ComponentScan({"com.workday.assignment.mashup"})
public class ApplicationConfig {
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
