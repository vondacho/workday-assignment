package com.workday.assignment.mashup;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Integration of the Twitter4j Java API
 * 
 * @author olivier
 */
@Component
public class TwitterSearchImpl implements TwitterSearch {
	static final Logger logger = LoggerFactory.getLogger(TwitterSearchImpl.class);
	
	static final int NO_RESULT_LIMIT = 0;
		
	@Value("${twitterConsumerKey:}")
	private String consumerKey;
	@Value("${twitterConsumerSecret:}")
	private String consumerSecret;
	@Value("${twitterAccessToken:}")
	private String accessToken;
	@Value("${twitterAccessTokenSecret:}")
	private String accessTokenSecret;
	@Value("${twitterResultSizeLimit:0}")
	private Integer resultSizeLimit;
	
	private Twitter root;

	/**
	 * @param terms a list of search terms
	 * @return a list of found tweets (result size limited by configuration)
	 */
	public List<Status> process(String...terms) {
		try {
			Twitter twitter = init();
			QueryResult result = twitter.search(buildQuery(resultSizeLimit, terms));
			logger.info("twitter api: rate limit status: {}", result.getRateLimitStatus());
			List<Status> tweets = result.getTweets();
			logger.debug("twitter api: tweets count: {}", tweets.size());
			return tweets;
		} catch (TwitterException e) {
			if (e.exceededRateLimitation())
				logger.info("twitter api: rate limit status: {}", e.getRateLimitStatus());
			else
				logger.info("twitter api: exception occurred: {}", e.getMessage());
			return Collections.emptyList();
		}
	}
	
	/**
	 * @return a Twitter root for next requests to the Twitter storage
	 */
	private Twitter init() {
		return root = (root != null ? root :
			new TwitterFactory(
				new ConfigurationBuilder().
					setDebugEnabled(false).
					setOAuthConsumerKey(consumerKey).
					setOAuthConsumerSecret(consumerSecret).
					setOAuthAccessToken(accessToken).
					setOAuthAccessTokenSecret(accessTokenSecret).
					setJSONStoreEnabled(true).build()).getInstance());
	}
	
	/**
	 * @param resultSizeLimit
	 * @param terms
	 * @return a query object based on the given search terms
	 */
	private Query buildQuery(int resultSizeLimit, String...terms) {
		Query query = new Query(StringUtils.arrayToDelimitedString(terms, "OR")).resultType(Query.RECENT);
		if (resultSizeLimit > NO_RESULT_LIMIT)
			query.count(resultSizeLimit);
		logger.debug("twitter api: query: {}", query.toString());
		return query;
	}
}
