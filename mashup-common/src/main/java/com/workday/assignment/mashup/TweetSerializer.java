package com.workday.assignment.mashup;

import java.lang.reflect.Type;

import twitter4j.Status;
import twitter4j.TwitterObjectFactory;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * Gson serializer for <code>Twitter4j.Status</code>
 * 
 * @author olivier
 */
public class TweetSerializer implements JsonSerializer<Status> {
	@Override
	public JsonElement serialize(final Status tweet, final Type typeOfSrc, final JsonSerializationContext context) {
		String json = TwitterObjectFactory.getRawJSON(tweet);
		return json != null ? new JsonPrimitive(json) : JsonNull.INSTANCE;
	}
	
	public static GsonBuilder register(GsonBuilder builder) {
		return builder.registerTypeAdapter(Status.class, new TweetSerializer());
	}
}
