package com.workday.assignment.mashup;

import java.util.List;

/**
 * Sortable project summary (project name, related recent tweets)
 * 
 * @author olivier
 */
public class ProjectSummary implements Comparable<ProjectSummary> {
	private String project;
	private List<?> tweets;
	
	public ProjectSummary(String project, List<?> tweets) {
		this.project = project;
		this.tweets = tweets;
	}

	public String getProject() {
		return project;
	}

	public List<?> getTweets() {
		return tweets;
	}

	@Override
	public int compareTo(ProjectSummary other) {
		return project.compareToIgnoreCase(other.project);
	}
}
