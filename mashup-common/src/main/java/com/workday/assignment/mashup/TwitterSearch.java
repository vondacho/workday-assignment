package com.workday.assignment.mashup;

import java.util.List;

import twitter4j.Status;

/**
 * Interface for a Twitter search
 * 
 * @author olivier
 */
public interface TwitterSearch {
	/**
	 * @param terms a list of search terms
	 * @return a list of found tweets (result size limited by configuration)
	 */
	public List<Status> process(String...terms);
}
