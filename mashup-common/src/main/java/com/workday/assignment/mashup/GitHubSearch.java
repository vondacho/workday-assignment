package com.workday.assignment.mashup;


/**
 * Interface for a GitHub Search
 * 
 * @author olivier
 */
public interface GitHubSearch {

	/**
	 * @param terms a list of search terms
	 * @return an iterable of found GitHub projects (result size limited by configuration)
	 */
	public Iterable<GHRepo> process(String...terms);
}
