package com.workday.assignment.mashup;

import org.kohsuke.github.GHRepository;

import com.workday.assignment.util.Wrapper;

/**
 * Wrapper on <code>GHRepository</code> class which provides equality on project name
 * 
 * @author olivier
 */
public class GHRepo extends Wrapper<GHRepository, String> {
	
	public GHRepo(GHRepository t) {
		super(t, GHRepository::getName);
	}
	
	public String getFullName() {
		return unwrap().getFullName();
	}
	
	public String getName() {
		return unwrap().getName();
	}
}
