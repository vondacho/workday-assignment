package com.workday.assignment.mashup;

import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;

import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GHRepositorySearchBuilder;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.PagedSearchIterable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Integration of the GitHub Java API by Kohsuke
 * 
 * @author olivier
 */
@Component
public class GitHubSearchImpl implements GitHubSearch {
	static final Logger logger = LoggerFactory.getLogger(GitHubSearchImpl.class);
	
	@Value("${githubLogin:}")
	private String login;
	@Value("${githubPassword:}")
	private String password;
	@Value("${githubResultSizeLimit:0}")
	private Integer resultSizeLimit;

	private GitHub root;

	/**
	 * @param terms a list of search terms
	 * @return an iterable of found GitHub projects (result size limited by configuration)
	 */
	public Iterable<GHRepo> process(String...terms) {
		try {
			connect();
			logger.info("github api: current rate limit is {}", root.getRateLimit());
			GHRepositorySearchBuilder builder = root.searchRepositories();
			for (String term: terms) {
				builder = builder.q(term);
			}
			logger.debug("github api: query");
			return from(builder.list(), resultSizeLimit);
		} catch (IOException e) {
			logger.info("github api: exception occurred: {}", e.getMessage());
			return Collections.emptyList();
		}
	}
	
	/**
	 * @return a GitHub root as encapsulation of the Internet connection to the GitHub storage
	 * @throws IOException
	 */
	private GitHub connect() throws IOException {
		if (root == null) {
			if (StringUtils.isEmpty(login)) {
				logger.info("github api: anonymous connection");
				root = GitHub.connectAnonymously();
			}
			else {
				logger.info("github api: connection {}", login);
				root = GitHub.connectUsingPassword(login, password);
			}
		}
		return root;
	}
	
	/**
	 * @param resulting iterator of found GitHub projects
	 * @return wrapper which limits the size of the final iterable
	 */
	private Iterable<GHRepo> from(final PagedSearchIterable<GHRepository> result, int resultSizeLimit) {
		return new Iterable<GHRepo>() {
			final Iterator<GHRepository> resultIt = result.iterator();
			@Override
			public Iterator<GHRepo> iterator() {
				return new Iterator<GHRepo>() {
					private int index = 0;
					@Override
					public boolean hasNext() {
						try {
							return resultSizeLimit > 0 ? (index < resultSizeLimit ? resultIt.hasNext() : false) : resultIt.hasNext();
						} catch (Throwable e) {
							return false;
						}
					}

					@Override
					public GHRepo next() {
						GHRepository repo = resultIt.next();
						index++;
						return new GHRepo(repo);
					}
				};
			}
		};
	}
}
